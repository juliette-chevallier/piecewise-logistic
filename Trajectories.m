function T = Trajectories(fixed)

    t0 = 0 ;
     
    
    function y = g0(t,z_pop,l)
%{
   compute gamma_0(t) with parameters (g0init,g0escap,...)
      if l = 1, compute only the decreasing logistic,
      if l = 2, only the increasing logistic and
      if l = 0 (default value), both logitic.
    
 % INPUTs :
    t : double(~,1)
    z_pop : double(1,5) -- hiden variables for the global population
        z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
            g0init, g0escap, g0fin, tR, t1 : double
    t0, nu : double
    l : double, *optional* -- l = 1, 2 or 0

 % GLOBALs :
    fixed : cell(1,6) -- fixed hyperparameters of the model
        fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
            sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
    
 % OUTPUT :
    y : double(~,1)
%}
        
        nu = fixed(6) ;
        if nargin < 3
            l = 0 ;
        end

        % z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
        g0init = z_pop(1) ;
        g0escap = z_pop(2) ;
        g0fin = z_pop(3) ;
        tR = z_pop(4) ;
        t1 = z_pop(5) ;

        if l == 1  % decreasing logistic only
            d0 = g0init-g0escap ;
            a = 2/(d0*(tR-t0))*log((d0-nu)/nu) ;
            b = (tR+t0)/(d0*(tR-t0))*log(nu/(d0-nu)) ;    
            y = (g0init + g0escap*exp(d0*(a*t+b))) ./ (1 + exp(d0*(a*t+b))) ;

        elseif l == 2  % increasing logistic only
            d0 = g0fin-g0escap ;
            c = 2/(d0*(t1-tR))*log((d0-nu)/nu) ;
            d = (tR+t1)/(d0*(t1-tR))*log(nu/(d0-nu)) ;
            y = (g0fin + g0escap*exp(-d0*(c*t+d))) ./ (1 + exp(-d0*(c*t+d))) ;

        elseif l == 0  % both logistic
            d0 = g0init-g0escap ;
            a = 2/(d0*(tR-t0))*log((d0-nu)/nu) ;
            b = (tR+t0)/(d0*(tR-t0))*log(nu/(d0-nu)) ;
            y = (g0init + g0escap*exp(d0*(a*t+b))) ./ (1 + exp(d0*(a*t+b))) .* (t<=tR) ;
            d0 = g0fin-g0escap ;
            c = 2/(d0*(t1-tR))*log((d0-nu)/nu) ;
            d = (tR+t1)/(d0*(t1-tR))*log(nu/(d0-nu)) ;
            y = y + (g0fin + g0escap*exp(-d0*(c*t+d))) ./ (1 + exp(-d0*(c*t+d))) .* (t>tR) ;
        end
    end
    T.g0 = @g0 ;
    
    
    function [yi,ti0,tiR,ti1] = gi(t,z_pop,P,gshift)
%{
    compute gamma_i(t) with parameters (P,z_pop,P,t0,nu)
    
 % INPUTs :
    t : double(~,1)
    z_pop : double(1,5) -- hiden variables for the global population
        z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
            g0init, g0escap, g0fin, tR, t1 : double
    P : double(p,p) -- P = (xi1,xi2,tau1,rho1,rho2,delta)
    t0, nu : double
    
 % OUTPUTs :
    yi : double(~,1) -- gamma_i(t) for individual i
    tiR, ti1 : double -- rupture and final time for individual i
%}
        
        if nargin < 4
            gshift = z_pop(2) ;
        end
        
        tR = z_pop(4) ;
        t1 = z_pop(5) ;
        yR = gshift+fixed(6) ;

        alpha1 = exp(P(1)) ;  
        alpha2 = exp(P(2)) ;
        tau1 = P(3) ;
        tau2 = tau1 + (1-alpha1)*(tR-t0)/alpha1 ;
        r1 = exp(P(4)) ;
        r2 = exp(P(5)) ;
        delta = P(6) ;

        ti0 = t0+tau1 ;
        tiR = t0+tau1+(tR-t0)/alpha1 ;
        ti1 = tR+tau2+(t1-tR)/alpha2 ;

        % decreasing logistic
        ti = alpha1*(t-t0-tau1)+t0 ;
        yi = 0*t + ( r1*(g0(ti,z_pop,1)-yR)+yR+delta ) .* (t<=tiR) .* (t>=ti0) ;
        yi(isnan(yi)) = 0 ;

        % increasing logistic
        ti = alpha2*(t-tR-tau2)+tR ;
        yi = yi + ( r2*(g0(ti,z_pop,2)-yR)+yR+delta ) .* (t>tiR) .* (t<=ti1) ;
        yi(isnan(yi)) = 0 ;

        % Extension before t0 and after ti1 for plotting
        yi1 = r2*(z_pop(3)-fixed(6)-yR)+yR+delta ;
        yi0 = r1*(z_pop(1)-fixed(6)-yR)+yR+delta ;
        yiR = yR+delta ;
        yi = yi + yi1*(t>ti1).*(t1>tR) + yiR*(t>=tiR).*(t1<tR) ...
            + yi0*(t<ti0).*(t0<tR) + yiR*(t<=tiR).*(t0>tR) ;
    end
    T.gi = @gi ;

end