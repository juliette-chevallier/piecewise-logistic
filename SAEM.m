
function theta = SAEM(theta_init,PL,sampler,maxIter,Nburnin)
%{

 % INPUTs :
    T : cell(1,N) -- observation times for all individuals
      T{i} : double(k_i,1) -- observation times for individual i
    Y : cell(1,N) -- observations for all individuals
      Y{i} : double(k_i,1) -- observations for individual i
    theta_init : cell(1,7) -- initialisation for the MCMC-SAEM
      theta_init = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
          g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
          Sigma : double(p,p)

    sampler -- choice of the sampler
        Methods of the HMwG_sampler class
        sampler = 'SRW_HMwG', 'Half_Block_SRW_HMwG', 'Block_SRW_HMwG' or 'Block_IS_HMwG'

    fixed : cell(1,6) -- fixed hyperparameters of the model
        fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
            sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
    prior : cell(1,14) -- prior on the parameters
        prior = [ g0init_bar_bar, s_init, g0escap_bar_bar, s_escap, g0fin_bar_bar, s_fin, ...
                    tR_bar_bar, s_R, t1_bar_bar, s_1, V, m_Sigma, v, m_sigma ]
            g0init_bar_bar, s_init, g0escap_bar_bar, s_escap, g0fin_bar_bar, s_fin : double
            tR_bar_bar, s_R, t1_bar_bar, s_1, m_Sigma, v, m_sigma : double
            V : douple(p,p)
 
    plt : double(4,1) -- plot parameters
        plt = [ plt_1, plt_2, plt_3, plt_4 ]
        plt_1 - Fig 1 : g0init, g0escap, g0fin, tR, t1, sigma
        plt_2 - Fig 2 : Mean acceptance rate
        plt_3 - Fig 3 : Sigma
        plt_4 - Fig 4 : Evolution over time
    
    maxIter : double -- nb of iterations in the SAEM
    Nburnin : double -- burning time in the SAEM

 % OUTPUT :
    theta_end : cell(1,7) -- 
        theta_endt = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
          g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
          Sigma : double(p,p)
%}  

    if nargin < 4
        maxIter = 50000 ;
    end
    if maxIter < 5
        Nburnin = .5*maxIter ;
    end
    
    T_Gibbs = 0 ; T_SAEM = 0 ;
    global acpt_gibbs ; acpt_gibbs = zeros(1,6) ;

    
        %%%%%%%%%%%%%%%%%%%%%%%%
        %%%% Initialization %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%
    
    % theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
    % P = (xi1,xi2,tau,rho1,rho2,delta)
    % z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
    [theta,z_pop,P] = PL.Init(theta_init) ;
   
    % Plot parameters
    [~, ~, scale] = sampler(z_pop, P, theta) ;
    MP = Mk_Plts(scale,PL.plt) ;
    Nbatch_plot = 5 ;
    global iter ; iter = 0 ; 
    PL.Plt_Evol(z_pop,P)
    
    T_SAEM = T_SAEM - cputime ;
    for iter = 1:maxIter

            %%%%%%%%%%%%%%%%%%%%%%%
            %%%%%% Sampling %%%%%%%
            %%%%%%%%%%%%%%%%%%%%%%%

        T_Gibbs = T_Gibbs - cputime ;
        [z_pop, P] = sampler(z_pop, P, theta) ;
        T_Gibbs = T_Gibbs + cputime ;
        
      
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%%% SA, M and Visualisation %%%%
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        % theta = [ g0init, g0escap, g0fin, tR, t1, Sigma, sigma ]
        if iter <= Nburnin
            eps = 1 ;
        else
            eps = 1/(iter-Nburnin)^(0.65) ;
        end
%         eps = (iter<=Nburnin) + 1/(iter-Nburnin)^(0.65)*(iter>Nburnin) ; 
        theta = PL.SA_M(theta,z_pop,P,eps) ;
%         theta{7}
%         eig(theta{6})
        
        if (mod(iter,Nbatch_plot)==0) 
            PL.Plt_Evol(z_pop,P)
            MP.Plt(theta)
        end
    end
    hold off

    T_SAEM = T_SAEM + cputime;
    fprintf(' \n')
    fprintf('SAEM took %f seconds, i.e %f minutes. \n',T_SAEM,T_SAEM/60) ;
    fprintf('HMwG took %f seconds, i.e %f minutes. \n',T_Gibbs,T_Gibbs/60) ;
end
