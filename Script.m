clear ; load('Data/******.mat')

t0 = 0 ;
p_ind = 6 ;


for i = 1:data(end,3)
    tgt = find(data(:,3) == i) ;
    T{i} = data(tgt,4)' ;
    Y{i} = data(tgt,5)' ;
end



%% theta_init, fixed, prior and proposal

for i = 1:n
    t1(i) = T{i}(end) ;
    g0init(i) = Y{i}(1) ;
    g0escap(i) = min(Y{i}) ;
    g0fin(i) = Y{i}(end) ;
end

% theta_init
g0init_bar_init = mean(g0init) 
g0escap_bar_init = mean(g0escap) 
g0fin_bar_init = mean(g0fin)  
tR_bar_init = .5*mean(t1) 
t1_bar_init = mean(t1) 
Sigma_init = .1*eye(p_ind) ;
sigma_init = 1 ;
theta_init = { g0init_bar_init, g0escap_bar_init, g0fin_bar_init, ...
    tR_bar_init, t1_bar_init, Sigma_init, sigma_init } ;

% prior
g0init_bar_bar = mean(g0init) ;
s_init = 1 ;
g0escap_bar_bar = mean(g0escap) ;
s_escap = 1 ;
g0fin_bar_bar = mean(g0fin) ;
s_fin = 1 ;
tR_bar_bar = .5*mean(t1) ;
s_R = 1 ;
t1_bar_bar = mean(t1) ;
s_1 = 1 ;
V = .1*eye(p) ; %V(end,end) = 10 ; %eig(V)
m_Sigma = 6 ;
v = .1 ;
m_sigma = 2 ;
prior = { g0init_bar_bar, s_init, g0escap_bar_bar, s_escap, g0fin_bar_bar, s_fin, ...
                    tR_bar_bar, s_R, t1_bar_bar, s_1, V, m_Sigma, v, m_sigma } ;



%% HMwG-SAEM

plt_1 = 0 ;  % Fig 1 : g0init, g0escap, g0fin, tR, t1, sigma
plt_2 = 0 ;  % Fig 2 : Mean acceptance rate
plt_3 = 0 ;  % Fig 3 : Sigma
plt_4 = 0 ;  % Fig 4 : Evolution over time
plt = [ plt_1, plt_2, plt_3, plt_4 ] ;
global nb ; nb = 5 ;

PL = Piecewise_Logistic(T,Y,fixed,prior,plt) ;
HM = HMwG_sampler(PL,fixed,prop,Nmean) ;
%{ 
    sampler = 'SRW_HMwG', 'Half_Block_SRW_HMwG', 'Block_SRW_HMwG' ...
                'Half_Block_IS_HMwG' 
%}
sampler = HM.Block_SRW_HMwG ;

theta = SAEM(theta_init,PL,sampler,maxIter,Nburnin) ;




