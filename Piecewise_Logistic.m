function PL = Piecewise_Logistic(T,Y,fixed,prior,plt)

    global acpt_gibbs ;
    global iter ;
    
    if nargin<5
        plt = zeros(1,4) ;
    end
    
    t0 = 0 ;
    p = 6 ;
    acpt_gibbs = zeros(1,6) ;
    n = length(T) ;  % nb of individuals
    k = sum(cellfun(@(u) size(u,2),Y)) ;  % total nb of observations
    
    PL.n = n ;
    PL.plt = plt ;
    PL.Y = Y ;
    
    
        %%%%%%%%%%%%%%%%%%%%%%
        %%%% Trajectories %%%%
        %%%%%%%%%%%%%%%%%%%%%%
    
    function y = g0(t,z_pop,l)
    %{
       compute gamma_0(t) with parameters (g0init,g0escap,...)
          if l = 1, compute only the decreasing logistic,
          if l = 2, only the increasing logistic and
          if l = 0 (default value), both logitic.

     % INPUTs :
        t : double(~,1)
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        l : double, *optional* -- l = 1, 2 or 0

     % OUTPUT :
        y : double(~,1)
    %}

        nu = fixed(6) ;
        if nargin < 3
            l = 0 ;
        end

        % z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
        g0init = z_pop(1) ;
        g0escap = z_pop(2) ;
        g0fin = z_pop(3) ;
        tR = z_pop(4) ;
        t1 = z_pop(5) ;

        if l == 1  % decreasing logistic only
            d0 = g0init-g0escap ;
            a = 2/(d0*(tR-t0))*log((d0-nu)/nu) ;
            b = (tR+t0)/(d0*(tR-t0))*log(nu/(d0-nu)) ;    
            y = (g0init + g0escap*exp(d0*(a*t+b))) ./ (1 + exp(d0*(a*t+b))) ;

        elseif l == 2  % increasing logistic only
            d0 = g0fin-g0escap ;
            c = 2/(d0*(t1-tR))*log((d0-nu)/nu) ;
            d = (tR+t1)/(d0*(t1-tR))*log(nu/(d0-nu)) ;
            y = (g0fin + g0escap*exp(-d0*(c*t+d))) ./ (1 + exp(-d0*(c*t+d))) ;

        elseif l == 0  % both logistic
            d0 = g0init-g0escap ;
            a = 2/(d0*(tR-t0))*log((d0-nu)/nu) ;
            b = (tR+t0)/(d0*(tR-t0))*log(nu/(d0-nu)) ;
            y = (g0init + g0escap*exp(d0*(a*t+b))) ./ (1 + exp(d0*(a*t+b))) .* (t<=tR) ;
            d0 = g0fin-g0escap ;
            c = 2/(d0*(t1-tR))*log((d0-nu)/nu) ;
            d = (tR+t1)/(d0*(t1-tR))*log(nu/(d0-nu)) ;
            y = y + (g0fin + g0escap*exp(-d0*(c*t+d))) ./ (1 + exp(-d0*(c*t+d))) .* (t>tR) ;
        end
    end
    PL.g0 = @g0 ;
    
        
    function [yi,ti0,tiR,ti1] = gi(t,z_pop,P)
        %{
            compute gamma_i(t) with parameters (P,z_pop,P)

         % INPUTs :
            t : double(~,1)
            z_pop : double(1,5) -- hiden variables for the global population
                z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                    g0init, g0escap, g0fin, tR, t1 : double
            P : double(p,p) -- P = (xi1,xi2,tau1,rho1,rho2,delta)

         % OUTPUTs :
            yi : double(~,1) -- gamma_i(t) for individual i
            tiR, ti1 : double -- rupture and final time for individual i
        %}
        tR = z_pop(4) ;
        t1 = z_pop(5) ;
        yR = z_pop(2)+fixed(6) ;

        alpha1 = exp(P(1)) ;  
        alpha2 = exp(P(2)) ;
        tau1 = P(3) ;
        tau2 = tau1 + (1-alpha1)*(tR-t0)/alpha1 ;
        r1 = exp(P(4)) ;
        r2 = exp(P(5)) ;
        delta = P(6) ;

        ti0 = t0+tau1 ;
        tiR = t0+tau1+(tR-t0)/alpha1 ;
        ti1 = tR+tau2+(t1-tR)/alpha2 ;

        % decreasing logistic
        ti = alpha1*(t-t0-tau1)+t0 ;
        yi = 0*t + ( r1*(g0(ti,z_pop,1)-yR)+yR+delta ) .* (t<=tiR) .* (t>=ti0) ;
        yi(isnan(yi)) = 0 ;

        % increasing logistic
        ti = alpha2*(t-tR-tau2)+tR ;
        yi = yi + ( r2*(g0(ti,z_pop,2)-yR)+yR+delta ) .* (t>tiR) .* (t<=ti1) ;
        yi(isnan(yi)) = 0 ;

        % Extension before t0 and after ti1 for plotting
        yi1 = r2*(z_pop(3)-fixed(6)-yR)+yR+delta ;
        yi0 = r1*(z_pop(1)-fixed(6)-yR)+yR+delta ;
        yiR = yR+delta ;
        yi = yi + yi1*(t>ti1).*(t1>tR) + yiR*(t>=tiR).*(t1<tR) ...
            + yi0*(t<ti0).*(t0<tR) + yiR*(t<=tiR).*(t0>tR) ;
    end
    PL.gi = @gi ;

        
    function r = q_ind(z_pop,Pi,i)
        r = (Y{i}-gi(T{i},z_pop,Pi))*(Y{i}-gi(T{i},z_pop,Pi))' ;
    end
    PL.q_ind = @q_ind ;
    
    
    function r = q_pop(z_pop,P)
        r = 0 ;
        for i = 1:n
            r = r + (Y{i}-gi(T{i},z_pop,P(:,i)))*(Y{i}-gi(T{i},z_pop,P(:,i)))' ;
        end
    end
    PL.q_pop = @q_pop ;
       
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% SAEM : Initialization %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    function [theta,z_pop,P,S] = Init(theta_init)
        theta = theta_init ;
        
        P = zeros(p,n) ;
        z_pop = zeros(1,5) ;
        for i = 1:5
            z_pop(i) = theta_init{i} ;
        end
    end
    PL.Init = @Init ;
    
    
    % Exhaustive statistics
    S = cell(1,7) ;
    for i = 1:7
        S{i} = 0 ;
    end
    S{6} = zeros(p,p) ;
    PL.S = S ;
 
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%% SAEM : Computation and Visualisation %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
    function theta = SA_M(theta,z_pop,P,eps)
        % Stochastic Approximation
        S{1} = S{1} + eps.*( z_pop(1) - S{1} ) ;
        S{2} = S{2} + eps.*( z_pop(2) - S{2} ) ;
        S{3} = S{3} + eps.*( z_pop(3) - S{3} ) ;
        S{4} = S{4} + eps.*( z_pop(4) - S{4} ) ;
        S{5} = S{5} + eps.*( z_pop(5) - S{5} ) ;
        S{6} = S{6} + eps.*( 1/n*P*P' - S{6} ) ;
        S{7} = S{7} + eps.*( 1/k*q_pop(z_pop,P) - S{7} ) ;
        
        % Maximization
        theta{1} = ( S{1}/(fixed(1)^2)+prior{1}/(prior{2}^2) )/( (1/(fixed(1)^2))+(1/(prior{2}^2)) ) ;
        theta{2} = ( S{2}/(fixed(2)^2)+prior{3}/(prior{4}^2) )/( (1/(fixed(2)^2))+(1/(prior{4}^2)) ) ;
        theta{3} = ( S{3}/(fixed(3)^2)+prior{5}/(prior{6}^2) )/( (1/(fixed(3)^2))+(1/(prior{6}^2)) ) ;
        theta{4} = ( S{4}/(fixed(4)^2)+prior{7}/(prior{8}^2) )/( (1/(fixed(4)^2))+(1/(prior{8}^2)) ) ;
        theta{5} = ( S{5}/(fixed(5)^2)+prior{9}/(prior{10}^2) )/( (1/(fixed(5)^2))+(1/(prior{10}^2)) ) ;
        theta{6} = ( n*S{6}+prior{12}*prior{11} )/( n+prior{12} ) ;
        theta{7} = sqrt(( k*S{7}+prior{14}*prior{13}^2 )/( k+prior{14} )) ;
    end
    PL.SA_M = @SA_M ;
    
    
    function Plt_Evol(z_pop,P)
        figure(4) ; clf ; hold off
        cmap4 = lines(nb) ;
        plot(t_pop,y_pop,'--k','linewidth',2) ;
        hold on
        plot(t_pop,g0(t_pop,z_pop),'k','linewidth',2) ;
        for i = 1:nb
            plot(t_ind{i},y_ind{i},'--','color',cmap4(i,:)) ;
            [~,ti0,~,ti1] = gi(t_pop,z_pop,P(:,i)) ;
            t = ti0:.01:ti1 ;
            plot(t,gi(t,z_pop,P(:,i)),'color',cmap4(i,:)) ;
            plot(T{i},Y{i},'+','color',cmap4(i,:)) ;
        end
        axis([t_min t_max y_min-5 y_max+5])
        title(sprintf('iteration %u',iter))
        pause(1e-3)
    end

    function Do_Nothing(x,y)
    end
    PL.Do_Nothing = @Do_Nothing ;
    
    
    global tP ; global tz_pop ; global nb ;
    if plt(4) 
        t_pop = 0:0.01:1000 ;
        y_pop = g0(t_pop,tz_pop) ;
        t_min = 0 ; t_max = 1000 ;
        y_min = min(y_pop) ; y_max = max(y_pop) ;
        for i = 1:nb
            [~,ti0,~,ti1] = gi(t_pop,tz_pop,tP(:,i)) ;
            t_ind{i} = ti0:.01:ti1 ;
            y_ind{i} = gi(t_ind{i},tz_pop,tP(:,i)) ;
            y_min = min(y_min,min(Y{i})) ; y_max = max(y_max,max(Y{i})) ;
            t_min = min(t_min,max(T{i})) ; t_max = max(t_max,max(T{i})) ;
        end
        PL.Plt_Evol = @Plt_Evol ;
    else
        PL.Plt_Evol = @Do_Nothing ;
    end
    
end


