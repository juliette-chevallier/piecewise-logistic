(Matlab) Codes associated to the piecewise logistic curve model 

See Script.m for an example
 

--

Learning spatiotemporal piecewise-geodesic trajectories from longitudinal manifold-valued data. Juliette Chevallier, Stéphane Oudard and Stéphanie Allassonnière. Advances in Neural Information Processing Systems 30, Long Beach, CA, USA, December 2017

A coherent framework for learning spatiotemporal piecewise- geodesic trajectories from longitudinal manifold-valued data. Juliette Chevallier, Vianney Debavelaere and Stéphanie Allassonnière. 2020. 