function MP = Mk_Plts(scale,plt)

    global acpt_gibbs ;
    global iter ;
    p = 6 ;
    l_title = { 'g_0^{init}','g_0^{escap}','g_0^{fin}' ,'t_R','t_1' } ;
    
   
    function Do_Nothing(x)
    end

    
        %%%%%%%%%%%%%%%%%%%%%%%%
        %%%% Initialization %%%%
        %%%%%%%%%%%%%%%%%%%%%%%%

    function Init_1
        figure(1) ; hold off ; clf ; hold on
        for i = 1:5
            subplot(2,3,i) ; hold on
            title(l_title{i})
        end
        subplot(2,3,6) ; hold on
        title('\sigma')
    end
    
    function Init_2a
        figure(2) ; hold off ; clf ; hold on
        subplot(1,2,1) ; hold on
        title('Acceptance rate - z_{pop}')
        subplot(1,2,2) ; hold on
        title('Acceptance rate - P')
    end
    
    function Init_2b
        figure(2) ; hold off ; clf ; hold on
        for i = 1:5
            subplot(2,3,i) ; hold on
            title(strcat('Acceptance rate - ',' ',l_title{i}))
        end
        subplot(2,3,6) ; hold on
        title('Acceptance rate - P')
    end
    
    function Init_3
        figure(3) ; hold off ; clf ; hold on
        l = 0 ;
        for i = 1:p
            for j = 1:p
                l = l+1 ; subplot(p,p,l) ; hold on
                title(sprintf('i = %u, j = %u',i,j))
            end
        end
    end

    
        %%%%%%%%%%%%%%%%%%%%%
        %%%% Build plots %%%%
        %%%%%%%%%%%%%%%%%%%%%

    function Mk_Plt_1(theta)
        figure(1) ; cmap1 = colormap(lines(6)) ;
        for i = 1:5
            subplot(2,3,i)
            plot(iter,theta{i},'.','linewidth',2,'color',cmap1(i,:)) ;
        end
        subplot(2,3,6) ;
        plot(iter,theta{7},'.','linewidth',2,'color',cmap1(6,:)) ;
        drawnow
    end

    function Mk_Plt_2a
        figure(2) ; cmap2 = colormap(lines(2)) ;
        subplot(1,2,1) ;
        plot(iter,acpt_gibbs(1)/(scale(1)*iter),'.','linewidth',2,'color',cmap2(1,:)) ;
        subplot(1,2,2) ;
        plot(iter,acpt_gibbs(2)/(scale(2)*iter),'.','linewidth',2,'color',cmap2(2,:)) ;
        drawnow 
    end
        
    function Mk_Plt_2b    
        figure(2) ; cmap2 = colormap(lines(6)) ;
        for i = 1:5
            subplot(2,3,i) ;
            plot(iter,acpt_gibbs(i)/(scale(1)*iter),'.','linewidth',2,'color',cmap2(i,:)) ;
        end
        subplot(2,3,6) ;
        plot(iter,acpt_gibbs(6)/(scale(2)*iter),'.','linewidth',2,'color',cmap2(6,:)) ;
        drawnow 
    end
        
    function Mk_Plt_3(theta)
        figure(3) ; cmap3 = colormap(lines(p*p)) ;
        l = 0 ;
        for i = 1:p
            for j = 1:p
                l = l+1 ; subplot(p,p,l) ;
                plot(iter,theta{6}(i,j),'.','linewidth',2,'color',cmap3(k,:)) ;
            end
        end
        drawnow
    end

    function Plt_1(theta) % Plt = 1, 2a, 3
        Mk_Plt_1(theta) ;
        Mk_Plt_2a ;
        Mk_Plt_3(theta) ;
    end
    
    function Plt_2(theta) % Plt = 1, 2b, 3
        Mk_Plt_1(theta) ;
        Mk_Plt_2b ;
        Mk_Plt_3(theta) ;
    end
    
    function Plt_3(theta) % Plt = 1, 2a
        Mk_Plt_1(theta) ;
        Mk_Plt_2a ;
    end
    
    function Plt_4(theta) % Plt = 1, 2b
        Mk_Plt_1(theta) ;
        Mk_Plt_2b ;
    end
    
    function Plt_5(theta) % Plt = 1, 3
        Mk_Plt_1(theta) ;
        Mk_Plt_3(theta) ;
    end
    
    function Plt_6(theta) % Plt = 1
        Mk_Plt_1(theta) ;
    end
    
    function Plt_7(theta) % Plt = 2a, 3
        Mk_Plt_2a ;
        Mk_Plt_3(theta) ;
    end
    
    function Plt_8(theta) % Plt = 2b, 3
        Mk_Plt_2b ;
        Mk_Plt_3(theta) ;
    end
    
    function Plt_9(theta) % Plt = 2a
        Mk_Plt_2a ;
    end
    
    function Plt_10(theta) % Plt = 2b
        Mk_Plt_2b ;
    end
    
    function Plt_11(theta) % Plt = 3
        Mk_Plt_3(theta) ;
    end
    
    
        %%%%%%%%%%%%%%%%%%%%%
        %%%% Assignation %%%%
        %%%%%%%%%%%%%%%%%%%%%
    
    if plt(1)
        Init_1 ;
        if plt(2)
            if scale(1) == 1
                Init_2a ;
                if plt(3)
                    Init_3 ;
                    MP.Plt = @Plt_1 ;
                else
                    MP.Plt = @Plt_3 ;
                end
            else
                Init_2b ;
                if plt(3)
                    Init_3 ;
                    MP.Plt = @Plt_2 ;
                else
                    MP.Plt = @Plt_4 ;
                end
            end
        elseif plt(3)
            Init_3 ;
            MP.Plt = @Plt_5 ;
        else
            MP.Plt = @Plt_6 ;
        end
    elseif plt(2)
        if scale(1) == 1
            Init_2a ;
            if plt(3)
                Init_3 ;
                MP.Plt = @Plt_7 ;
            else
                MP.Plt = @Plt_9 ;
            end
        else
            Init_2b ;
            if plt(3)
                Init_3 ;
                MP.Plt = @Plt_8 ;
            else
                MP.Plt = @Plt_10 ;
            end
        end
    elseif plt(3)
        Init_3 ;
        MP.Plt = @Plt_11 ;
    else
        MP.Plt = @Do_Nothing ;
    end
    
end





