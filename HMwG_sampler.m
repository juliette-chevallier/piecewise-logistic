function HM = HMwG_sampler(PL,fixed,prop,Nmean)
%{
    sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
    different Hasting-Metropolis within Gibbs sampler are build :
        - Symmetric Random Walk
        - Half-Block Symmetric Random Walk 
        - Block Symmetric Random Walk
        - Half-Block Independant Sampling

 % INPUTs :
    fixed : cell(1,6) -- fixed hyperparameters of the model
        fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
            sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
    prop : double(1,6) -- proposal (variances) for the sampling
        prop = [ zeta_pop, zeta_P ]
        zeta_pop : double(1,5) -- proposal variance for population
            zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
            zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
        zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

 % GLOBALs :
    acpt_gibbs : double(1,6) -- mean acceptance rate in Gibbs sampler
        acpt_gibbs = [ acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P ]
            acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P : double
    
 % OUTPUTs :
    z_pop : double(1,5) -- hiden variables for the global population
        z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
            g0init, g0escap, g0fin, tR, t1 : double
    P : double(p,p)
%}

    global acpt_gibbs ;
    global iter ;
    p = 6 ;
    Nbatch = 50 ;
    
    global acpt ;
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%% Symmetric Random Walk %%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    function [z_pop, P, scale, alpha] = SRW_HMwG(z_pop, P, theta)
    %{
        sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
        use a Symmetric Random Walk Hasting-Metropolis within Gibbs sampler

     % INPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
        theta : cell(1,7) -- current value of theta
          theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
              g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
              Sigma : double(p,p)

     % GLOBALs :
        fixed : cell(1,6) -- fixed hyperparameters of the model
            fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
                sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
        prop : double(1,6) -- proposal (variances) for the sampling
            prop = [ zeta_pop, zeta_P ]
            zeta_pop : double(1,5) -- proposal variance for population
                zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
                zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
            zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

     % OUTPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
    %}
        
        alpha = zeros(1,PL.n+5) ;

        % Individuals 
        if iter > Nmean
            E = eye(p) ;
            for i = 1:PL.n
                for j = 1:p
                    P_star = P(:,i) + prop(6:end)'*randn.*E(:,j) ;
                    alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                    alpha(i+5) = alpha(i+5)/(theta{7}^2) ...
                       + dot(P_star,theta{6}\P_star) - dot(P(:,i),theta{6}\P(:,i)) ;        
                    U = rand ;
                    if j == 6
                        acpt = [acpt ; i,P_star(j),log(U) <= -.5*alpha(i+5)] ;
                    end
                    if log(U) <= -.5*alpha(i+5)
                        P(j,i) = P_star(j) ;
                        acpt_gibbs(6) = acpt_gibbs(6)+1 ;
                    end
                end
            end
        end

        % Population
        E = eye(5) ;
        for j = 1:5
            z_star = z_pop + prop(1:5)*randn.*E(j,:) ;
            alpha(j) = PL.q_pop(z_star,P) - PL.q_pop(z_pop,P) ;
            alpha(j) = alpha(j)/(theta{7}^2) ...
                + ( (z_star(j)-theta{j})^2 - (z_pop(j)-theta{j})^2 )/(fixed(j)^2) ;
            U = rand ;
            if log(U) <= -.5*alpha(j)
                z_pop(j) = z_star(j) ;
                acpt_gibbs(j) = acpt_gibbs(j)+1 ;
            end
        end
        
        scale = [5,PL.n*p] ;
        if mod(iter,Nbatch) == 0
            fprintf('iter = %6.0u  |  g0init = %1.3f    g0escap = %1.3f    g0fin = %1.3f \n', ...
                iter, acpt_gibbs(1)/(scale(1)*iter), acpt_gibbs(2)/(scale(1)*iter), acpt_gibbs(3)/(scale(1)*iter)) ; 
            fprintf('               |      tR = %1.3f         t1 = %1.3f        P = %1.3f \n', ...
                acpt_gibbs(4)/(scale(1)*iter), acpt_gibbs(5)/(scale(1)*iter), acpt_gibbs(6)/(scale(2)*iter)) ;
        end
    end
    HM.SRW_HMwG = @SRW_HMwG ;
    
    
    
    function [z_pop, P, scale, alpha] = Bias_SRW_HMwG(z_pop, P, theta)
    %{
        sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
        use a Symmetric Random Walk Hasting-Metropolis within Gibbs sampler

     % INPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
        theta : cell(1,7) -- current value of theta
          theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
              g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
              Sigma : double(p,p)

     % GLOBALs :
        fixed : cell(1,6) -- fixed hyperparameters of the model
            fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
                sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
        prop : double(1,6) -- proposal (variances) for the sampling
            prop = [ zeta_pop, zeta_P ]
            zeta_pop : double(1,5) -- proposal variance for population
                zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
                zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
            zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

     % OUTPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
    %}
        
        alpha = zeros(1,PL.n+5) ;

        % Only delta
        if iter <= Nmean
            E = eye(p) ;
            for i = 1:PL.n
                for j = 1:(p-1)
                    P_star = P(:,i) + prop(6:end)'*randn.*E(:,j) ;
                    alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                    alpha(i+5) = alpha(i+5)/(theta{7}^2) ...
                       + dot(P_star,theta{6}\P_star) - dot(P(:,i),theta{6}\P(:,i)) ;        
                    U = rand ;
                    if j == 6
                        acpt = [acpt ; i,P_star(j),log(U) <= -.5*alpha(i+5)] ;
                    end
                    if log(U) <= -.5*alpha(i+5)
                        P(j,i) = P_star(j) ;
                        acpt_gibbs(6) = acpt_gibbs(6)+1 ;
                    end
                end
                P(end,i) = min(PL.Y{i})-z_pop(2)-fixed(6) ;
            end
        end
        
        % Individuals 
        if iter > Nmean
            E = eye(p) ;
            for i = 1:PL.n
                for j = 1:p
                    P_star = P(:,i) + prop(6:end)'*randn.*E(:,j) ;
                    alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                    alpha(i+5) = alpha(i+5)/(theta{7}^2) ...
                       + dot(P_star,theta{6}\P_star) - dot(P(:,i),theta{6}\P(:,i)) ;        
                    U = rand ;
                    if j == 6
                        acpt = [acpt ; i,P_star(j),log(U) <= -.5*alpha(i+5)] ;
                    end
                    if log(U) <= -.5*alpha(i+5)
                        P(j,i) = P_star(j) ;
                        acpt_gibbs(6) = acpt_gibbs(6)+1 ;
                    end
                end
            end
        end

        % Population
        E = eye(5) ;
        for j = 1:5
            z_star = z_pop + prop(1:5)*randn.*E(j,:) ;
            alpha(j) = PL.q_pop(z_star,P) - PL.q_pop(z_pop,P) ;
            alpha(j) = alpha(j)/(theta{7}^2) ...
                + ( (z_star(j)-theta{j})^2 - (z_pop(j)-theta{j})^2 )/(fixed(j)^2) ;
            U = rand ;
            if log(U) <= -.5*alpha(j)
                z_pop(j) = z_star(j) ;
                acpt_gibbs(j) = acpt_gibbs(j)+1 ;
            end
        end
        
        scale = [5,PL.n*p] ;
        if mod(iter,Nbatch) == 0
            fprintf('iter = %6.0u  |  g0init = %1.3f    g0escap = %1.3f    g0fin = %1.3f \n', ...
                iter, acpt_gibbs(1)/(scale(1)*iter), acpt_gibbs(2)/(scale(1)*iter), acpt_gibbs(3)/(scale(1)*iter)) ; 
            fprintf('               |      tR = %1.3f         t1 = %1.3f        P = %1.3f \n', ...
                acpt_gibbs(4)/(scale(1)*iter), acpt_gibbs(5)/(scale(1)*iter), acpt_gibbs(6)/(scale(2)*iter)) ;
        end
    end
    HM.Bias_SRW_HMwG = @Bias_SRW_HMwG ;
    
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%% Half-Block Symmetric Random Walk %%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    function [z_pop, P, scale, alpha] = Half_Block_SRW_HMwG(z_pop, P, theta)
    %{
        sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
        use a Half-Block Random Walk Hasting-Metropolis within Gibbs sampler

     % INPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
        theta : cell(1,7) -- current value of theta
          theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
              g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
              Sigma : double(p,p)

     % GLOBALs :
        acpt_gibbs : double(1,6) -- mean acceptance rate in Gibbs sampler
            acpt_gibbs = [ acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P ]
                acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P : double
        fixed : cell(1,6) -- fixed hyperparameters of the model
            fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
                sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
        prop : double(1,6) -- proposal (variances) for the sampling
            prop = [ zeta_pop, zeta_P ]
            zeta_pop : double(1,5) -- proposal variance for population
                zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
                zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
            zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

     % OUTPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
    %}
  
        alpha = zeros(1,PL.n+5) ;

        % Individuals
        if iter > Nmean
            for i = 1:PL.n
%                 P_star = P(:,i) + chol(diag(prop(6:end)))'*randn(p,1) ;
                P_star = mvnrnd(P(:,i),diag(prop(6:end)))' ;
                alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                alpha(i+5) = alpha(i+5)/(theta{7}^2) ...
                   + dot(P_star,theta{6}\P_star) - dot(P(:,i),theta{6}\P(:,i)) ;        
                U = rand ;
                if log(U) <= -.5*alpha(i+5)
                    P(:,i) = P_star ;
                    acpt_gibbs(6) = acpt_gibbs(6)+1 ;
                end
            end
        end
        
        % Population
        E = eye(5) ;
        for j = 1:5
            z_star = z_pop + prop(1:5)*randn.*E(j,:) ;
            alpha(j) = PL.q_pop(z_star,P) - PL.q_pop(z_pop,P) ;
            alpha(j) = alpha(j)/(theta{7}^2) ...
                + ( (z_star(j)-theta{j})^2 - (z_pop(j)-theta{j})^2 )/(fixed(j)^2) ;
            U = rand ;
            if log(U) <= -.5*alpha(j)
                z_pop(j) = z_star(j) ;
                acpt_gibbs(j) = acpt_gibbs(j)+1 ;
            end
        end
        
        scale = [5,PL.n] ;
        if mod(iter,Nbatch) == 0
            fprintf('iter = %6.0u  |  g0init = %1.3f    g0escap = %1.3f    g0fin = %1.3f \n', ...
                iter, acpt_gibbs(1)/(scale(1)*iter), acpt_gibbs(2)/(scale(1)*iter), acpt_gibbs(3)/(scale(1)*iter)) ; 
            fprintf('               |      tR = %1.3f         t1 = %1.3f        P = %1.3f \n', ...
                acpt_gibbs(4)/(scale(1)*iter), acpt_gibbs(5)/(scale(1)*iter), acpt_gibbs(6)/(scale(2)*iter)) ;
        end
    end
    HM.Half_Block_SRW_HMwG = @Half_Block_SRW_HMwG ;
    
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%% Block Symmetric Random Walk %%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function [z_pop, P, scale, alpha] = Block_SRW_HMwG(z_pop, P, theta)
    %{
        sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
        use a Block Symmetric Random Walk Hasting-Metropolis within Gibbs sampler

     % INPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
        theta : cell(1,7) -- current value of theta
          theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
              g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
              Sigma : double(p,p)

     % GLOBALs :
        acpt_gibbs : double(1,6) -- mean acceptance rate in Gibbs sampler
            acpt_gibbs = [ acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P ]
                acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P : double
        fixed : cell(1,6) -- fixed hyperparameters of the model
            fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
                sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
        prop : double(1,6) -- proposal (variances) for the sampling
            prop = [ zeta_pop, zeta_P ]
            zeta_pop : double(1,5) -- proposal variance for population
                zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
                zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
            zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

     % OUTPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
    %}

        alpha = zeros(1,PL.n+5) ;

        % Individuals
        if iter > Nmean
            for i = 1:PL.n
%                 P_star = P(:,i) + chol(diag(prop(6:end)))'*randn(p,1) ;
                P_star = mvnrnd(P(:,i),diag(prop(6:end)))' ;
                alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                alpha(i+5) = alpha(i+5)/(theta{7}^2) ...
                   + dot(P_star,theta{6}\P_star) - dot(P(:,i),theta{6}\P(:,i)) ;        
                U = rand ;
                if log(U) <= -.5*alpha(i+5)
                    P(:,i) = P_star ;
                    acpt_gibbs(2) = acpt_gibbs(2)+1 ;
                end
            end
        end

        % Population
        z_star = z_pop + randn(1,5)*chol(diag(prop(1:5)))' ;
        % z_star = mvnrnd(z_pop,diag(prop(1:5))) ;
        j = 1 ;
        alpha(j) = PL.q_pop(z_star,P) - PL.q_pop(z_pop,P) ;
        alpha(j) = alpha(j)/(theta{7}^2) ...
            + ( (z_star(j)-theta{j})^2 - (z_pop(j)-theta{j})^2 )/(fixed(j)^2) ;
        U = rand ;
        if log(U) <= -.5*alpha(j)
            z_pop = z_star ;
            acpt_gibbs(1) = acpt_gibbs(1)+1 ;
        end
        
        scale = [1,PL.n] ;
        if mod(iter,Nbatch) == 0
            fprintf('iter = %6.0u  |  z_pop = %1.3f    P = %1.3f    \n', ...
                iter, acpt_gibbs(1)/(scale(1)*iter), acpt_gibbs(2)/(scale(2)*iter))
        end
    end
    HM.Block_SRW_HMwG = @Block_SRW_HMwG ;
    
    
    
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%% Half-Block Independant Sampling %%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
    function [z_pop, P, scale, alpha] = Block_IS_HMwG(z_pop, P, theta)
    %{
        sample (z_pop,z_i) = [ g0init, g0escap, g0fin, tR, t1, P ]
        use a Block Independant Sampling Hasting-Metropolis within Gibbs sampler

     % INPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
        theta : cell(1,7) -- current value of theta
          theta = [ g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, Sigma, sigma ]
              g0init_bar, g0escap_bar, g0fin_bar, tR_bar, t1_bar, sigma : double
              Sigma : double(p,p)

     % GLOBALs :
        acpt_gibbs : double(1,6) -- mean acceptance rate in Gibbs sampler
            acpt_gibbs = [ acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P ]
                acpt_init acpt_escpa, acpt_fin, acpt_R, acpt_1, acpt_P : double
        fixed : cell(1,6) -- fixed hyperparameters of the model
            fixed = [ sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu ]
                sigma_init, sigma_escap, sigma_fin, sigma_R, sigma_1, nu : double
        prop : double(1,6) -- proposal (variances) for the sampling
            prop = [ zeta_pop, zeta_P ]
            zeta_pop : double(1,5) -- proposal variance for population
                zeta_pop = [zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1]
                zeta_g0init, zeta_g0escap, zeta_g0fin, zeta_tR, zeta_t1 : double
            zeta_P : double -- proposal variance for individuals : zeta_P*eye(p)

     % OUTPUTs :
        z_pop : double(1,5) -- hiden variables for the global population
            z_pop = [ g0init, g0escap, g0fin, tR, t1 ]
                g0init, g0escap, g0fin, tR, t1 : double
        P : double(p,p)
    %}

        alpha = zeros(1,n+5) ;

        % Individuals
        if iter > Nmean
            for i = 1:PL.n
                P_star = mvnrnd(P(:,i),theta{6})' ;
                alpha(i+5) = PL.q_ind(z_pop,P_star,i) - PL.q_ind(z_pop,P(:,i),i) ;
                alpha(i+5) = alpha(i+5)/(theta{7}^2) ;        
                U = rand ;
                if log(U) <= -.5*alpha(i+5)
                    P(:,i) = P_star ;
                    acpt_gibbs(6) = acpt_gibbs(6) + 1 ;
                end
            end
        end

        % Population
        E = eye(5) ;
        for j = 1:5
            z_star = z_pop + fixed(1:5)*randn.*E(j,:) ;
            alpha(j) = PL.q_pop(z_star,P) - PL.q_pop(z_pop,P) ;
            alpha(j) = alpha(j)/(theta{7}^2) ;
            U = rand ;
            if log(U) <= -.5*alpha(j)
                z_pop(j) = z_star(j) ;
                acpt_gibbs(j) = acpt_gibbs(j)+1 ;
            end
        end
        
        scale = [5,PL.n] ;
        if mod(iter,Nbatch) == 0
            fprintf('iter = %6.0u  |  g0init = %1.3f    g0escap = %1.3f    g0fin = %1.3f \n', ...
                iter, acpt_gibbs(1)/(scale(1)*iter), acpt_gibbs(2)/(scale(1)*iter), acpt_gibbs(3)/(scale(1)*iter)) ; 
            fprintf('               |      tR = %1.3f         t1 = %1.3f        P = %1.3f \n', ...
                acpt_gibbs(4)/(scale(1)*iter), acpt_gibbs(5)/(scale(1)*iter), acpt_gibbs(6)/(scale(2)*iter)) ;
        end
    end
    HM.Block_IS_HMwG = @Block_IS_HMwG ;
    
end